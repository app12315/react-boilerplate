const OFF = 0,
  WARN = 1,
  ERROR = 2;
module.exports = {
  extends: ['prettier', 'plugin:react/recommended'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 9,
    sourceType: 'module',
    allowImportExportEverywhere: false,
    codeFrame: false,
    ecmaFeatures: {
      globalReturn: true,
      impliedStrict: true,
      jsx: true,
      arrowFunction: true,
    },
  },
  plugins: ['react', 'jest', 'prettier'],
  env: {
    es6: true,
    browser: true,
  },
  rules: {
    'react/jsx-filename-extension': [WARN, { extensions: ['.js', '.jsx'] }],
    'no-unused-vars': [
      'error',
      { vars: 'all', args: 'after-used', ignoreRestSiblings: false },
    ],
    'react/prop-types': OFF,
  },
};
