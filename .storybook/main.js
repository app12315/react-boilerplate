const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');

module.exports = {
  stories: ['../stories/**/*.story.@(jsx|mdx)'],
  addons: ['@storybook/addon-essentials'],
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    config.module.rules.push({
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: ['babel-loader', 'eslint-loader'],
    });

    config.module.rules.push({
      test: /\.(sa|sc|c)ss$/,
      use: [
        'css-hot-loader',
        MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader',
      ],
    });

    config.module.rules.push({
      test: /\.(ttf|woff2|woff|eot|svg)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]',
          },
        },
      ],
    });

    config.module.rules.push({
      exclude: /node_modules/,
      test: /\.(png|jpg|svg)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'images/[folder]/[name].[ext]',
            ignore: '/node_modules/',
          },
        },
      ],
    });

    config.module.rules.push({
      test: /\.json$/,
      type: 'javascript/auto',
      exclude: /(node_modules|bower_components|stories|src)/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'locales/[folder]/[name].[ext]',
          },
        },
      ],
    });

    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: 'styles.css',
      }),
    );
    config.plugins.push(
      new webpack.DefinePlugin({
        PRODUCTION: JSON.stringify('storybook'),
      }),
    );

    // Return the altered config
    return config;
  },
};
