import '../index.js';
import React from 'react';
import { StoryProvider } from '../stories/utils/storyProvider';

export const decorators = [
  (Story) => (
    <StoryProvider>
      <Story />
    </StoryProvider>
  ),
];
