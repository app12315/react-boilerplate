# react-boilerplate

## Remote dev env (recommended)

https://code.visualstudio.com/docs/remote/containers-advanced#_use-open-repository-in-a-container

1. Open VS-Code
2. Press F1
3. Select `Remote-Containers: Open Repository in a Container...`
4. Enter Repo url

## Local dev env (Legacy )

### Prerequisites

<details><summary>node >= 14 </summary>
<h3>Linux</h3>
<pre><code>sudo apt update &&
sudo apt install nodejs npm
</code></pre>
<hr>
<h3>Windows:</h3>
<a href="https://nodejs.org/en/"> Installer </a>
</details>

---

## Commands

### Install:

Run `yarn install`

### Run:

> For development build run `yarn start` (Local Live Server)
>
> For production build run `yarn build`

### Storybook

> For Storybook run `yarn storybook`

### Docs

> Generate docs with `yarn docs`

### Build docker container

- Run production build
- Build docker image with `docker build -t frontend:latest .`

---
