import React from 'react';
import PropTypes from 'prop-types';

/**
 * Example Component.
 *
 * @component
 * @example
 * const version = '0.0.1';
 * const date = new Date();
 * return (
 *   <Example version={version} date={date} />
 * )
 */
export default function Example({ version, date }) {
  return <p>{`Version: ${version} Date: ${date}`}</p>;
}

Example.propTypes = {
  /**
   * App version
   */
  version: PropTypes.string.isRequired,
  /**
   * Current date
   */
  date: PropTypes.any.isRequired,
};

Example.defaultProps = {
  version: '0.0.1',
  date: new Date(),
};
