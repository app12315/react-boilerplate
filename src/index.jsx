import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createStore, compose } from 'redux';
import { Provider } from 'react-redux';

import rootReducer from './redux/rootReducer';
import App from './app';

const storeEnhancers =
  (typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

// Initialize store
const store = createStore(rootReducer, storeEnhancers());

if (PRODUCTION !== 'storybook') {
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <Route component={App} />
      </Router>
    </Provider>,
    document.getElementById('app'),
  );
}
