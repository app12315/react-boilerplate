const webpack = require('webpack');
const { merge } = require('webpack-merge');
const baseConfig = require('./base.config');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
path = require('path');

module.exports = merge(baseConfig, {
  devtool: 'eval-source-map',
  mode: 'development',
  plugins: [
    new BrowserSyncPlugin(
      {
        host: '127.0.0.1',
        port: 3000,
        proxy: 'http://127.0.0.1:9000/',
        open: false,
      },
      {
        // prevent BrowserSync from reloading the page
        // and let Webpack Dev Server take care of this
        reload: false,
        injectCss: true,
      },
    ),
    // Env variabels
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(false),
    }),
  ],

  devServer: {
    contentBase: path.join(__dirname, '/../dist'),
    historyApiFallback: true,
    inline: true,
    port: 9000,
    host: '0.0.0.0',
    liveReload: true,
  },
});
